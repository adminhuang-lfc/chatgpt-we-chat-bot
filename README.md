# ChatgptWeChatBot

## 最新警告：自2023.3.5日起，有微信限制登陆风险，此项目将永久终止开发！！！

#### 介绍
自己开发的ChatGPT微信接入项目<br>
python新手，希望多多支持<br>
基于ntchat 微信3.6.0<br>
windows系统<br>
 **ChatGPT的API请提前自备** 

#### 其他说明
ChatGPT中国注册教程：https://zhuanlan.zhihu.com/p/590240014<br>
ChatGPT开放API获取教程：https://blog.csdn.net/biggbang/article/details/128656086<br>
微信3.6.0安装包：https://github.com/tom-snow/wechat-windows-versions/releases/download/v3.6.0.18/WeChatSetup-3.6.0.18.exe<br>

#### 软件运行平台
Windows7或以上版本 x86或x86-64平台<br>
推荐使用python3.8版本<br>
 **微信必须使用3.6.0并禁用自动更新** <br>

#### 安装教程

1.您首先需要安装python解释器以及微信<br>
2.在系统命令行终端运行命令安装依赖项目3.6.0版本<br>

```
pip install psutil
pip install ntchat
pip install requests
```
3.安装完成后，下载代码，打开main.py，修改其中的配置（ **26至38行** ）<br>
4.运行auto_run.bat 登陆微信 开始使用<br>
(如果auto_run.bat不可用，请手动启动main.py<br>
备注：<br>
    使用“禁止微信自动更新”来让微信彻底不自动更新<br>
    check_wechat_run.py代码可以保持微信的运行，请按需使用<br>

#### 软件功能

1.接入chatgpt，进行自动回复<br>
2.带有黑名单功能 可以屏蔽某些人<br>
3.启动时可以给大号发送一个消息<br>
4.带有详细的日志记录<br>

#### 参与贡献

Admin Huang

#### 感谢
ntchat库<br>
https://gitcode.net/mirrors/smallevilbeast/ntchat<br>


### 注意事项
1.您必须使用微信3.6.0版本<br>
2.您必须使用windows7以上系统 不支持ARM系统<br>
3. **使用本程序造成的一切不良后果（限制登陆，封号等）与开发者及代码托管平台无关！** <br>
4.建议不要用大号，注册个小号来搞<br>

